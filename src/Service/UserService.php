<?php

namespace Angle\Service;

use Angle\Model\UserModel;

class UserService
{
    private $userModel;

    /**
     * UserService constructor.
     * @param UserModel $userModel
     */
    public function __construct(UserModel $userModel)
    {
        $this->userModel = $userModel;
    }


    /**
     * Prüfen ob Username oder E-Mailadresse schon existieren
     *
     * @param string $username
     * @param string $email
     * @return array
     */
    public function checkUserNameOrEmailExist(string $username, string $email): array
    {
        $checkUsernameExist = $this->userModel->checkUsernameAvailable($username);
        $checkEmailExist = $this->userModel->checkEmailAvailable($email);

        return [
            'username' => $checkUsernameExist,
            'email' => $checkEmailExist
        ];
    }


    /**
     * Passwort vergessen - E-Mailadresse prüfen
     *
     * @param null|string $email
     * @return bool
     */
    public function forgetPasswordCheck(?string $email): bool
    {
        $validateEmail = filter_var($email, FILTER_VALIDATE_EMAIL);

        if ($validateEmail) {
            $checkEmailAvailable = $this->userModel->checkEmailAvailable($email);

            if ($checkEmailAvailable) {
                return $this->createForgetPasswordCode($email);
            }
            else return false;
        }
        else return false;
    }


    /**
     * Eindeutige ID (String) erstellen, in DB speichern & diesen per Mail versenden
     *
     * @param string $email
     * @return bool
     */
    public function createForgetPasswordCode(string $email): bool
    {
        do {
            $uniqueId = uniqid();
            $checkPwCodeExists = $this->userModel->checkPasswordCodeAlreadyExists($uniqueId);
        } while ($checkPwCodeExists);

        $insertForgetPasswordCode = $this->userModel->insertForgetPasswordCode($email, $uniqueId);

        if ($insertForgetPasswordCode) {
            return $this->sendForgetPasswordCode($email, $uniqueId);
        }
        else return false;
    }


    /**
     * Prüfen ob Passwortcode in Db vorhanden & noch gültig ist
     *
     * @param string $uniqueId
     * @return bool
     */
    public function checkValidResetPasswordCode(string $uniqueId): bool
    {
        $uniqueId = trim($uniqueId);

        if (strlen($uniqueId) === 13) {
            $data = $this->userModel->findUserDataByPasswordCode($uniqueId);

            $passwordCodeCreatedAt = strtotime($data['link_created_at']);
            $dateTimeNow = strtotime(date('Y-m-d H:i:s'));
            $diff = $dateTimeNow - $passwordCodeCreatedAt;
            $dateDiff = floor($diff / 60);

            if ($dateDiff > 30)
                return false;
            else return true;

        }
        else return false;
    }


    /**
     * Prüfen ob Passwort & Password-Wiederholung übereinstimmen
     *
     * @param string $password
     * @param string $passwordWdh
     * @return array
     */
    public function checkValidNewPasswords(string $password, string $passwordWdh): array
    {
        $errors = [];

        if (trim($password) !== trim($passwordWdh))
            $errors[] = "Passwort und Passwort-Wiederholung stimmen nicht überein.";
        elseif ($password === "" || $passwordWdh === "")
            $errors[] = "Passwort oder Passwort-Wiederholung sind leer.";

        return $errors;
    }


    /**
     * Neues Passwort speichern & UniqueId + UniqueId-Erstellungsdatum entfernen
     *
     * @param string $password
     * @param string $uniqueId
     * @return bool
     */
    public function saveNewPassword(string $password, string $uniqueId): bool
    {
        return $this->userModel->saveNewPassword($password, $uniqueId);
    }


    /**
     * Passwort zurücksetzen - Code per Mail versenden
     *
     * @param string $email
     * @param string $uniqueId
     * @return bool
     */
    public function sendForgetPasswordCode(string $email, string $uniqueId): bool
    {
        return true;
    }


    /**
     * Benutzerdaten anhand einer E-Mailadresse holen
     *
     * @param string $email
     * @return array
     */
    public function getUserDataByEmail(string $email): array
    {
        return $this->userModel->findUserDataByEmail($email);
    }


    /**
     * Benutzerdaten anhand der BaufirmenID holen
     *
     * @param int $userId
     * @return array
     */
    public function getUserDataById(int $userId): array
    {
        return $this->userModel->findUserDataById($userId);
    }


    /**
     * Passwort ändern
     *
     * @param string $password
     * @param int $userId
     * @return bool
     */
    public function updatePassword(string $password, int $userId): bool
    {
        return $this->userModel->updatePassword($password, $userId);
    }


    /**
     * Profildaten bearbeiten
     *
     * @param string $username
     * @param string $email
     * @param int $userId
     * @return bool
     */
    public function updateProfileData(string $username, string $email, int $userId): bool
    {
        return $this->userModel->updateProfileData($username, $email, $userId);
    }



}