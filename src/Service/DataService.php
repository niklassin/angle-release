<?php

namespace Angle\Service;

use Angle\Model\DataModel;
use Instagram\Api;
use Instagram\Hydrator\Component\Feed;
use Instagram\Storage\CacheManager;

class DataService
{
    private $dataModel;

    public function __construct(DataModel $dataModel)
    {
        $this->dataModel = $dataModel;
    }


    /**
     * Anzahl von Newsartikeln holen
     *
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getNewsByLimit(int $limit, int $offset): array
    {
        return $this->dataModel->findNewsByLimit($limit, $offset);
    }


    public function convertNews($newsData): array
    {
        for ($i = 0; $i < count($newsData); $i++) {
            $newsData[$i]['news_content'] = strip_tags(html_entity_decode($newsData[$i]['news_content']));
        }

        return $newsData;
    }

    /**
     * Anzahl von Newsseiten ermitteln
     *
     * @return int
     */
    public function getNewsPagesNumber(): int
    {
        $pagesNumber = (int)$this->countAllNews();
        $pagesNumber = ceil($pagesNumber / 12);

        return $pagesNumber;
    }


    /**
     * Anzahl von Newsartikeln holen
     *
     * @return int
     */
    public function countAllNews(): int
    {
        return $this->dataModel->countAllNews();
    }


    public function getNewsArticleByUrl($article)
    {
        return $this->dataModel->findNewsArticleByUrl($article);
    }


    public function getLastFiveNews(): array
    {
        return $this->dataModel->findLastFiveNews();
    }


    /**
     * Alle WackerTV-Videos holen
     *
     * @return array
     */
    public function getAllVideos(): array
    {
        return $this->dataModel->findAllVideos();
    }


    public function getAllPictureGalleries()
    {
        $data = $this->dataModel->findAllPictureGalleries();

        for($i = 0; $i < count($data); $i++) {
            $imagesDir = glob('../public/gallery/'.$data[$i]["year"].'/'.$data[$i]["path"], GLOB_ONLYDIR);
            $randomfolder = $imagesDir[array_rand($imagesDir)];
            $images = glob($randomfolder . '/*.{jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF}', GLOB_BRACE);
            $randomImage = $images[array_rand($images)];
            $randomImage = str_replace('public/', '', $randomImage);

            $data[$i]['random_thumbnail'] = $randomImage;
            $data[$i]['pictures_number'] = count($images);
        }

        return $data;
    }


    public function getGalleryAlbumPictures($year, $album)
    {
        $imagesDir = glob('../public/gallery/'.$year.'/'.$album, GLOB_ONLYDIR);
        $randomfolder = $imagesDir[array_rand($imagesDir)];
        $images = glob($randomfolder . '/*.{jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF}', GLOB_BRACE);

        return $images;
    }

    public function getAlbumInfosByNameAndYear($album, $year)
    {
        return $this->dataModel->findAlbumInfosByNameAndYear($album, $year);
    }


    public function getTopNewsByLimit(int $limit)
    {
        return $this->dataModel->findTopNewsByLimit($limit);
    }


    public function getTopNewsWithCommentsByLimit(int $limit)
    {
        return $this->dataModel->findTopNewsWithCommentsByLimit($limit);
    }

    public function getNewsComments(int $newsId)
    {
        return $this->dataModel->findNewsComments($newsId);
    }

    /**
     * @param int $newsId
     * @return bool
     */
    public function updateNewsArticleCounter(int $newsId): bool
    {
        return $this->dataModel->updateNewsArticleCounter($newsId);
    }


    /**
     * @param int $newsId
     * @param int $commentId
     * @param int $userId
     * @return bool
     * @throws \Exception
     */
    public function checkNewsCommentByUserExists(int $newsId, int $commentId, int $userId)
    {
        $result = $this->dataModel->checkNewsCommentByUserExists($newsId, $commentId, $userId);

        if($result > 0)
            return true;
        else
            throw new \Exception('Der ausgewählte Kommentar gehört dir nicht und kann somit nicht gelöscht werden!', 400);
    }

    public function deleteNewsCommentByUser(int $newsId, int $commentId, int $userId)
    {
        return $this->dataModel->deleteNewsCommentByUser($newsId, $commentId, $userId);
    }



    public function splitUrlPath(string $url): array
    {
        $output = explode('/', trim($url, '/'));
        $data = [];
        $path = '';

        foreach ($output as $elem) {
            $path .= $elem.'/';

            $newPath = $path;
            $checkSlash = substr($newPath, -1);

            if($checkSlash === "/") {
                $newPath = substr($newPath, 0, -1);
            }

            $data[] = [
                'name' => $elem,
                'path' => $newPath
            ];
        }

        return $data;
        #echo implode(' &gt;&gt; ', $output);
    }

    public function getInstagramFeed(): Feed
    {
        $cache = new CacheManager('');
        $api = new Api($cache);
        $api->setUserName('wackergladbeck');
        $feed = $api->getFeed();

        return $feed;
    }


    public function formatInstagramFeed(array $feedArray): array
    {
        $feed = [];

        foreach ($feedArray as $feedArr) {
            $feedArr = (array)$feedArr;
            array_push($feed, $feedArr);
        }

        return $feed;
    }

    public function getAllGames(string $status = null, int $teamId = null)
    {
        $data = $this->dataModel->findAllGames($status, $teamId);

        for($i = 0; $i < count($data); $i++) {
            $dateObj = new \DateTime($data[$i]['game_date']);

            $day = $dateObj->format('d');
            $year = $dateObj->format('Y');
            $month = $this->getMonthNameByMonthNumber($dateObj->format('n'));
            $hour = $dateObj->format('H');
            $minutes = $dateObj->format('i');

            $convertDate = $day . '. ' . $month . ' ' . $year . ' - ' . $hour . ':' . $minutes;

            $data[$i]['format_game_date'] = $convertDate;
        }

        return $data;
    }

    public function getTeamLeagueStatistics(int $teamId)
    {
        $data = $this->dataModel->findAllGames(null, $teamId);

        $gamesCount = count($data);
        $points = 0;
        $goalsOwn = 0;
        $goalsRival = 0;
        $winsCount = 0;
        $drawCount = 0; # Unentschieden
        $defeatCount = 0; # Niederlage
        $yellowCardsCount = 0;
        $yellowRedCardsCount = 0;
        $redCardsCount = 0;
        $pointsHistory = [];

        for($i = 0; $i < count($data); $i++) {
            if($data[$i]['ground'] === 'home') {
                // Punkte berechnen
                if($data[$i]['goals_home'] > $data[$i]['goals_away']) {
                    $points += 3;
                    $winsCount += 1;
                    $pointsHistory[] = 3;
                }
                else if($data[$i]['goals_home'] === $data[$i]['goals_away']) {
                    $points += 1;
                    $drawCount += 1;
                    $pointsHistory[] = 1;
                }
                else {
                    $pointsHistory[] = 0;
                    $defeatCount += 1;
                }

                // Eigene Tore
                $goalsOwn += $data[$i]['goals_home'];

                // Gegner Tore
                $goalsRival += $data[$i]['goals_away'];
            }
            else {
                // Punkte berechnen
                if($data[$i]['goals_away'] > $data[$i]['goals_home']) {
                    $points += 3;
                    $winsCount += 1;
                    $pointsHistory[] = 3;
                }
                else if($data[$i]['goals_home'] === $data[$i]['goals_away']) {
                    $points += 1;
                    $drawCount += 1;
                    $pointsHistory[] = 1;
                }
                else {
                    $defeatCount += 1;
                    $pointsHistory[] = 0;
                }

                // Eigene Tore
                $goalsOwn += $data[$i]['goals_away'];

                // Gegner Tore
                $goalsRival += $data[$i]['goals_home'];
            }

            $yellowCardsCount += $data[$i]['yellow_card'];
            $yellowRedCardsCount += $data[$i]['yellow_red_card'];
            $redCardsCount += $data[$i]['red_card'];
        }

        $arr = [
            'gamesCount' => $gamesCount,
            'points' => $points,
            'goalsOwn' => $goalsOwn,
            'goalsRival' => $goalsRival,
            'winsCount' => $winsCount,
            'drawCount' => $drawCount,
            'defeatCount' => $defeatCount,
            'yellowCardsCount' => $yellowCardsCount,
            'yellowRedCardsCount' => $yellowRedCardsCount,
            'redCardsCount' => $redCardsCount
        ];

        return $arr;
    }

    public function getAllVorstandMembers()
    {
        $hauptVorstand = $this->dataModel->findAllVorstandMembersByCategorie('hauptvorstand');
        $jugendVorstand = $this->dataModel->findAllVorstandMembersByCategorie('jugendvorstand');
        $weitereAemter = $this->dataModel->findAllVorstandMembersByCategorie('weitere_aemter');

        $data = [
            'hauptvorstand' => $hauptVorstand,
            'jugendvorstand' => $jugendVorstand,
            'weitere_aemter' => $weitereAemter
        ];

        return $data;
    }


    public function getAllTrainer()
    {
        $seniorenTrainer = $this->dataModel->findAllTrainerByCategorie('s', 'f');
        $seniorenTrainer = $this->checkExistTrainerPictures($seniorenTrainer);
        $jugendTrainer = $this->dataModel->findAllTrainerByCategorie('j', null);
        $jugendTrainer = $this->checkExistTrainerPictures($jugendTrainer);

        $data = [
            'senioren_trainer' => $seniorenTrainer,
            'jugend_trainer' => $jugendTrainer,
        ];

        return $data;
    }


    public function checkExistTrainerPictures(array $data): array
    {
        for($i = 0; $i < count($data); $i++) {
            if(file_exists($_SERVER['DOCUMENT_ROOT'].'/assets/img/persons/'.$data[$i]['picture'])) {
                $data[$i]['picture'] = 'default-transparent.svg.png';
            }
        }

        return $data;
    }


    public function updateVideoViewsCounter(int $videoId)
    {
        if(is_numeric($videoId)) {
            return $this->dataModel->updateVideoViewsCounter($videoId);
        }
        else {
            throw new \Exception("Video-ID ungültig.", 400);
        }
    }

    public function insertContactData(?string $username, ?string $email, ?string $message)
    {
        $errors = [];

        if($username === "") {
            $errors[] = "Benutzername leer";
        }
        if($email === "") {
            $errors[] = "E-Mail leer";
        }
        if($message === "") {
            $errors[] = "Nachricht leer";
        }
        if(!filter_var($email, FILTER_VALIDATE_EMAIL) && $email !== "") {
            $errors[] = "Ungültige E-Mailadresse";
        }
        if(!preg_match("/^[a-zA-Z ]*$/",$username)) {
            $errors[] = "Only letters and white space allowed";
        }

        $username = $this->filterFormData($username);
        $email = $this->filterFormData($email);
        $message = $this->filterFormData($message);

        if(count($errors) >= 1 || !empty($errors)) {
            return [
                'errors' => $errors,
                'data' => [
                    'username' => $username,
                    'email' => $email,
                    'message' => $message
                ]
            ];
        }
        else {
            $this->dataModel->insertContactData($username, $email, $message);
            return true;
        }
    }

    public function filterFormData($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        $data = htmlentities($data);

        return $data;
    }

    public function sendContactFormMail(string $username, string $email, string $message)
    {
        $to = "kontakt@wacker-gladbeck.de";
        $subject = "Kontaktformular - Wacker Gladbeck";
        $text = '
            <html>
              <head>
                <title>Kontaktformular</title>
              </head>
              <body>
                <p>Benutzer: '.$username.'</p>
                <p>E-Mailadresse: '.$email.'</p>
                <p>Nachricht:<br />'.$message.'</p>
              </body>
            </html>
            ';

        // To send HTML mail, the Content-type header must be set
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

        // send email
        mail($to, $subject, $text, $headers);
    }

    public function getTeamPlayer($teamId)
    {
        return $this->dataModel->findTeamPlayer($teamId);
    }

    public function getTeamPage($teamId)
    {
        return $this->dataModel->findTeamPage($teamId);
    }

    public function getTeamStatistics($teamId)
    {
        $pointsPercent = 3 * 100 / 9;
        $result = [
            'pointsPercent' => $pointsPercent
        ];
    }

    public function getTeamTrainingTimes($teamId)
    {
        switch((int)$teamId) {
            case 1:
                $data = [
                    [
                        'day' => 'Dienstag',
                        'time' => '19:30 - 21:00 Uhr'
                    ],
                    [
                        'day' => 'Donnerstag',
                        'time' => '19:30 - 21:00 Uhr'
                    ]
                ];
                break;
            case 2:
                $data = [
                    [
                        'day' => 'Mittwoch',
                        'time' => '19:30 - 21:00 Uhr'
                    ],
                    [
                        'day' => 'Freitag',
                        'time' => '19:30 - 21:00 Uhr'
                    ]
                ];
                break;
            case 3:
                $data = [
                    [
                        'day' => 'Dienstag',
                        'time' => '19:30 - 21:00 Uhr'
                    ],
                    [
                        'day' => 'Donnerstag',
                        'time' => '19:30 - 21:00 Uhr'
                    ]
                ];
                break;
            case 4:
                $data = [
                    [
                        'day' => 'Mittwoch',
                        'time' => '19:30 - 21:00 Uhr'
                    ],
                    [
                        'day' => 'Freitag',
                        'time' => '19:30 - 21:00 Uhr'
                    ]
                ];
                break;
            case 5:
                $data = [
                    [
                        'day' => 'Donnerstag',
                        'time' => '19:00 - 20:30 Uhr'
                    ]
                ];
                break;
            case 6:
                $data = [
                    [
                        'day' => 'Dienstag',
                        'time' => '19:30 - 21:00 Uhr'
                    ],
                    [
                        'day' => 'Freitag',
                        'time' => '19:30 - 21:00 Uhr'
                    ]
                ];
                break;
            case 7:
                $data = [
                    [
                        'day' => 'Dienstag',
                        'time' => '18:00 - 19:30 Uhr'
                    ],
                    [
                        'day' => 'Donnerstag',
                        'time' => '18:00 - 19:30 Uhr'
                    ]
                ];
                break;
            case 8:
                $data = [
                    [
                        'day' => 'Dienstag',
                        'time' => '17:00 - 18:30 Uhr'
                    ],
                    [
                        'day' => 'Donnerstag',
                        'time' => '17:00 - 18:30 Uhr'
                    ]
                ];
                break;
            case 9:
                $data = [
                    [
                        'day' => 'Dienstag',
                        'time' => '17:00 - 18:30 Uhr'
                    ],
                    [
                        'day' => 'Donnerstag',
                        'time' => '17:00 - 18:30 Uhr'
                    ]
                ];
                break;
            case 10:
                $data = [
                    [
                        'day' => 'Dienstag',
                        'time' => '17:00 - 18:30 Uhr'
                    ],
                    [
                        'day' => 'Donnerstag',
                        'time' => '17:00 - 18:30 Uhr'
                    ]
                ];
                break;
            case 11:
                $data = [
                    [
                        'day' => 'Dienstag',
                        'time' => '17:00 - 18:00 Uhr'
                    ],
                    [
                        'day' => 'Donnerstag',
                        'time' => '17:00 - 18:00 Uhr'
                    ]
                ];
                break;
            case 12:
                $data = [
                    [
                        'day' => 'Montag',
                        'time' => '18:30 - 20:00 Uhr'
                    ],
                    [
                        'day' => 'Mittwoch',
                        'time' => '18:30 - 20:00 Uhr'
                    ],
                    [
                        'day' => 'Freitag',
                        'time' => '18:30 - 20:00 Uhr'
                    ]
                ];
        }

        return $data;
    }

    public function getTrainerByTeam($teamId)
    {
        return $this->dataModel->findTrainerByTeam($teamId);
    }

    public function getAllWackerMembers()
    {
        return $this->dataModel->findAllWackerMembers();
    }

    /**
     * Trainingszeiten von Senioren, Damen & Jugend holen
     * @return array
     */
    public function getTrainingTimes()
    {
        $trainingSeniorenDamen = $this->dataModel->findTrainingTimesBySeniorenDamen();
        $trainingJugend = $this->dataModel->findTrainingTimesByJugend();

        $result = [
            'seniorenDamen' => $trainingSeniorenDamen,
            'jugend' => $trainingJugend
        ];

        return $result;
    }


    public function insertOstercupRegistration($clubName, $team, $playerCount, $contactPersonName, $contactPersonEmail, $city): bool
    {
        $data = $this->dataModel->insertOstercupRegistration($clubName, $team, $playerCount, $contactPersonName, $contactPersonEmail, $city);

        if($data) {
            return true;
        }
        else {
            throw new \Exception("Ostercup-Anmeldung fehlgeschlagen.", 400);
        }
    }

    /**
     * Monatsnamen mittels Monatszahl holen
     * @param $monthNumber
     * @return string
     */
    public function getMonthNameByMonthNumber($monthNumber)
    {
        switch($monthNumber) {
            case 1:
                $monthName = 'Januar';
                break;
            case 2:
                $monthName = 'Februar';
                break;
            case 3:
                $monthName = 'März';
                break;
            case 4:
                $monthName = 'April';
                break;
            case 5:
                $monthName = 'Mai';
                break;
            case 6:
                $monthName = 'Juni';
                break;
            case 7:
                $monthName = 'Juli';
                break;
            case 8:
                $monthName = 'August';
                break;
            case 9:
                $monthName = 'September';
                break;
            case 10:
                $monthName = 'Oktober';
                break;
            case 11:
                $monthName = 'November';
                break;
            case 12:
                $monthName = 'Dezember';
                break;
            default:
                $monthName = '';
        }

        return $monthName;
    }


}