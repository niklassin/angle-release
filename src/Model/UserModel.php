<?php

namespace Angle\Model;

class UserModel
{
    private $pdo;

    /**
     * UserModel constructor.
     * @param \PDO $pdo
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }


    /**
     * Prüfen ob E-Mailadresse in DB vorhanden ist
     *
     * @param string $email
     * @return bool
     */
    public function checkEmailAvailable(string $email): bool
    {
        $stmt = $this->pdo->prepare("SELECT COUNT(*) FROM user WHERE email = :email");
        $stmt->bindParam(':email', $email);
        $stmt->execute();

        return $stmt->fetchColumn();
    }


    /**
     * Prüfen ob Benutzername in DB vorhanden ist
     *
     * @param string $username
     * @return bool
     */
    public function checkUsernameAvailable(string $username): bool
    {
        $stmt = $this->pdo->prepare("SELECT COUNT(*) FROM user WHERE username = :username");
        $stmt->bindParam(':username', $username);
        $stmt->execute();

        return $stmt->fetchColumn();
    }


    /**
     * Generierten Passwort vergessen-String speichern
     *
     * @param string $email
     * @param string $uniqueId
     * @return bool
     */
    public function insertForgetPasswordCode(string $email, string $uniqueId): bool
    {
        $date = date('Y-m-d H:i:s');

        $stmt = $this->pdo->prepare("UPDATE user SET link = :link, link_created_at = :created_at WHERE email = :email");
        $stmt->bindParam(':link', $uniqueId);
        $stmt->bindParam(':created_at', $date);
        $stmt->bindParam(':email', $email);

        return $stmt->execute();
    }


    /**
     * Benutzerdaten anhand d. Passwordcodes holen
     *
     * @param string $uniqueId
     * @return array
     */
    public function findUserDataByPasswordCode(string $uniqueId): array
    {
        $stmt = $this->pdo->prepare("SELECT * FROM user WHERE link = :link");
        $stmt->bindParam(':link', $uniqueId);

        if ($stmt->execute())
            return $stmt->fetch(\PDO::FETCH_ASSOC);
        else return array();
    }


    /**
     * Benutzerdaten anhand einer E-Mailadresse holen
     *
     * @param string $email
     * @return array
     */
    public function findUserDataByEmail(string $email): array
    {
        $stmt = $this->pdo->prepare("SELECT * FROM user WHERE email = :email");
        $stmt->bindParam(':email', $email);

        if ($stmt->execute())
            return $stmt->fetch(\PDO::FETCH_ASSOC);
        else return array();
    }


    /**
     * Benutzerdaten anhand der User-ID holen
     *
     * @param int $userId
     * @return array
     */
    public function findUserDataById(int $userId): array
    {
        $stmt = $this->pdo->prepare("SELECT * FROM user WHERE user_id = :user_id");
        $stmt->bindParam(':user_id', $userId);

        if ($stmt->execute())
            return $stmt->fetch(\PDO::FETCH_ASSOC);
        else return array();
    }


    /**
     * Prüfen ob Passwortcode bereits existiert
     *
     * @param string $uniqueId
     * @return bool
     */
    public function checkPasswordCodeAlreadyExists(string $uniqueId): bool
    {
        $stmt = $this->pdo->prepare("SELECT COUNT(*) FROM user WHERE link = :link");
        $stmt->bindParam(':link', $uniqueId);
        $stmt->execute();

        return $stmt->fetchColumn();
    }


    /**
     * Neues Passwort speichern & UniqueId + UniqueId-Erstellungsdatum entfernen
     *
     * @param string $password
     * @param string $uniqueId
     * @return bool
     */
    public function saveNewPassword(string $password, string $uniqueId): bool
    {
        $stmt = $this->pdo->prepare("UPDATE user SET password = :password, link = NULL, link_created_at = NULL WHERE link = :link");
        $stmt->bindParam(':password', $password);
        $stmt->bindParam(':link', $uniqueId);

        return $stmt->execute();
    }


    /**
     * Passwort ändern
     *
     * @param string $password
     * @param int $userId
     * @return bool
     */
    public function updatePassword(string $password, int $userId): bool
    {
        $stmt = $this->pdo->prepare("UPDATE user SET password = :password WHERE user_id = :user_id");
        $stmt->bindParam(':password', $password);
        $stmt->bindParam(':user_id', $userId);

        return $stmt->execute();
    }


    /**
     * Profildaten bearbeiten
     *
     * @param string $username
     * @param string $email
     * @param int $userId
     * @return bool
     */
    public function updateProfileData(string $username, string $email, int $userId): bool
    {
        $stmt = $this->pdo->prepare("UPDATE user SET username = :username, email = :email WHERE user_id = :user_id");
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':user_id', $userId);

        return $stmt->execute();
    }

}