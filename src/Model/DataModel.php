<?php

namespace Angle\Model;

class DataModel
{
    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }


    /**
     * LIMIT VARIABLE ÄNDERN!!!!!!!!!!!
     *
     * Anzahl von Newsartikeln holen
     *
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function findNewsByLimit(int $limit, int $offset): array
    {
        $stmt = $this->pdo->prepare("SELECT * FROM news_new ORDER BY news_id ASC LIMIT :limit OFFSET :offset");
        $stmt->bindValue(':limit', $limit, \PDO::PARAM_INT);
        $stmt->bindValue(':offset', $offset, \PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }


    public function findLastFiveNews(): array
    {
        $stmt = $this->pdo->prepare("SELECT * FROM news_new ORDER BY news_id DESC LIMIT 5");
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }


    /**
     * NOCH ÜBERARBEITEN !!!!
     *
     * Anzahl von Newsartikeln holen
     *
     * @return int
     */
    public function countAllNews(): int
    {
        $stmt = $this->pdo->query("SELECT title, post_name, date, content, CONCAT(LEFT(image, LENGTH(image) - LOCATE('.', REVERSE(image))),'-150x150.',SUBSTRING_INDEX(image, '.', -1)) AS image FROM ( SELECT p.post_title AS title, p.post_status AS 'status', p.post_date AS date, p.post_content AS content, p.post_name AS post_name, (SELECT `guid` FROM wp_posts WHERE id = m.meta_value) AS image FROM wp_posts p, wp_postmeta m WHERE p.post_type = 'post' AND p.post_status = 'publish' AND p.id = m.post_id AND m.meta_key = '_thumbnail_id' ORDER BY date DESC) TT");
        $stmt->execute();

        return $stmt->rowCount();
    }


    public function findNewsArticleByUrl($article)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM news_new WHERE url_path = :url_path");
        $stmt->bindParam(':url_path', $article);

        $stmt->execute();

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }


    /**
     * Alle WackerTV-Videos holen
     *
     * @return array
     */
    public function findAllVideos(): array
    {
        $stmt = $this->pdo->prepare("SELECT * FROM wackertv ORDER BY video_id DESC");
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }


    public function findAllPictureGalleries()
    {
        $stmt = $this->pdo->prepare("SELECT * FROM picture_gallery_albums ORDER BY album_id DESC");
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function findAlbumInfosByNameAndYear($album, $year)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM picture_gallery_albums WHERE path = :path AND year = :year");
        $stmt->bindParam(':path', $album);
        $stmt->bindParam(':year', $year);
        $stmt->execute();

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function findTopNewsByLimit(int $limit)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM news_new ORDER BY counter DESC LIMIT :limit");
        $stmt->bindValue(':limit', $limit, \PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }


    public function findTopNewsWithCommentsByLimit(int $limit)
    {
        $stmt = $this->pdo->prepare("SELECT n.*, COUNT(c.comment_id) as comments_count FROM news_new n LEFT JOIN comments c ON n.news_id = c.news_id GROUP BY n.news_id ORDER BY n.counter DESC LIMIT :limit");
        $stmt->bindValue(':limit', $limit, \PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function findNewsComments(int $newsId)
    {
        $stmt = $this->pdo->prepare("SELECT c.*, u.username FROM comments c LEFT JOIN user u ON c.user_id = u.user_id WHERE c.news_id = :news_id ORDER BY c.comment_id ASC");
        $stmt->bindParam(':news_id', $newsId);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function updateNewsArticleCounter(int $newsId): bool
    {
        $stmt = $this->pdo->prepare("UPDATE news_new SET counter = counter + 1 WHERE news_id = :news_id");
        $stmt->bindParam(':news_id', $newsId);

        return $stmt->execute();
    }

    public function checkNewsCommentByUserExists(int $newsId, int $commentId, int $userId)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM comments WHERE news_id = :news_id AND comment_id = :comment_id AND user_id = :user_id");
        $stmt->bindParam(':news_id', $newsId);
        $stmt->bindParam(':comment_id', $commentId);
        $stmt->bindParam(':user_id', $userId);
        $stmt->execute();

        return $stmt->rowCount();
    }

    public function deleteNewsCommentByUser(int $newsId, int $commentId, int $userId)
    {
        $stmt = $this->pdo->prepare("DELETE FROM comments WHERE news_id = :news_id AND comment_id = :comment_id AND user_id = :user_id");
        $stmt->bindParam(':news_id', $newsId);
        $stmt->bindParam(':comment_id', $commentId);
        $stmt->bindParam(':user_id', $userId);

        return $stmt->execute();
    }

    public function findAllVorstandMembersByCategorie($category)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM vorstand WHERE categorie = :categorie ORDER BY vorstand_id ASC");
        $stmt->bindParam(':categorie', $category);

        if ($stmt->execute()) {
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
    }


    public function findAllTrainerByCategorie($cat1, $cat2)
    {
        if (is_null($cat2)) {
            $stmt = $this->pdo->prepare("SELECT tr.*, te.team_name FROM trainer tr LEFT JOIN teams te ON tr.team_id = te.team_id WHERE tr.team_typ = :category1 ORDER BY tr.sort_id ASC");
            $stmt->bindParam(':category1', $cat1);
        } else {
            $stmt = $this->pdo->prepare("SELECT tr.*, te.team_name FROM trainer tr LEFT JOIN teams te ON tr.team_id = te.team_id  WHERE tr.team_typ = :category1 OR tr.team_typ = :category2 ORDER BY tr.sort_id ASC");
            $stmt->bindParam(':category1', $cat1);
            $stmt->bindParam(':category2', $cat2);
        }

        if ($stmt->execute()) {
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
    }


    public function updateVideoViewsCounter($videoId)
    {
        $stmt = $this->pdo->prepare("UPDATE wackertv SET views = views + 1 WHERE video_id = :video_id");
        $stmt->bindParam(':video_id', $videoId);

        return $stmt->execute();
    }

    public function insertContactData(string $username, string $email, string $message)
    {
        $date = date("Y-m-d");

        $stmt = $this->pdo->prepare("INSERT INTO contact_form (contact_name, email, message, contact_date, status) VALUES (:contact_name, :email, :message, :contact_date, '0')");
        $stmt->bindParam(':contact_name', $username);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':message', $message);
        $stmt->bindParam(':contact_date', $date);

        return $stmt->execute();
    }

    public function findTeamPlayer($teamId)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM player WHERE team_id = :team_id");
        $stmt->bindParam(':team_id', $teamId);

        if ($stmt->execute()) {
            $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $data;
        }
    }

    public function findTeamPage($teamId)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM team_page WHERE team_id = :team_id");
        $stmt->bindParam(':team_id', $teamId);

        if ($stmt->execute()) {
            $data = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $data;
        }
    }

    public function findTrainerByTeam($teamId)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM trainer WHERE team_id = :team_id ORDER BY sort_id ASC");
        $stmt->bindParam(':team_id', $teamId);

        if ($stmt->execute()) {
            $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $data;
        }
    }

    public function findAllWackerMembers()
    {
        $stmt = $this->pdo->prepare("SELECT * FROM wacker_team ORDER BY member_id ASC");

        if ($stmt->execute()) {
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
    }

    /**
     * Trainingszeiten von Senioren & Damen
     * @return array
     */
    public function findTrainingTimesBySeniorenDamen()
    {
        $stmt = $this->pdo->prepare("SELECT * FROM training_times WHERE team_category = 's' OR team_category = 'f' ORDER BY sort_id ASC");
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }


    /**
     * Trainingszeiten von Jugend
     * @return array
     */
    public function findTrainingTimesByJugend()
    {
        $stmt = $this->pdo->prepare("SELECT * FROM training_times WHERE team_category = 'j' ORDER BY sort_id ASC");
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function findAllGames(string $status = null, int $teamId = null)
    {
        $sql = "SELECT ga.*, te.team_label_name FROM games ga LEFT JOIN teams te ON ga.team_id = te.team_id WHERE ga.game_id IS NOT NULL";

        if(!empty($status)) {
            $sql .= " AND ga.status = :status";
        }

        if(!empty($teamId)) {
            $sql .= " AND ga.team_id = :team_id";
        }

        $stmt = $this->pdo->prepare($sql);

        if(!empty($status)) {
            $stmt->bindParam(':status', $status);
        }

        if(!empty($teamId)) {
            $stmt->bindParam(':team_id', $teamId);
        }

        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }


    public function insertOstercupRegistration($clubName, $team, $playerCount, $contactPersonName, $contactPersonEmail, $city): bool
    {
        $ostercup_date = "2019";

        $stmt = $this->pdo->prepare("INSERT INTO ostercup_registration (club_name, team, player_count, contact_person_name, contact_person_email, city, ostercup_date) VALUES (:club_name, :team, :player_count, :contact_person_name, :contact_person_email, :city, :ostercup_date)");
        $stmt->bindParam(':club_name', $clubName);
        $stmt->bindParam(':team', $team);
        $stmt->bindParam(':player_count', $playerCount);
        $stmt->bindParam(':contact_person_name', $contactPersonName);
        $stmt->bindParam(':contact_person_email', $contactPersonEmail);
        $stmt->bindParam(':city', $city);
        $stmt->bindParam(':ostercup_date', $ostercup_date);

        return $stmt->execute();
    }


}