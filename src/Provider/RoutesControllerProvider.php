<?php

namespace Angle\Provider;

use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Angle\Controller\IndexController;
use Angle\Controller\UserController;
use Angle\Service\DataService;
use Angle\Service\UserService;
use Angle\Model\DataModel;
use Angle\Model\UserModel;
use Angle\Provider\UserProvider;

class RoutesControllerProvider implements ControllerProviderInterface
{

    public function connect(Application $app)
    {
        $app['indexController'] = function($c) {
            return new IndexController($c['dataService'], $c['userService']);
        };

        $app['userController'] = function($c) {
            return new UserController($c['userService']);
        };

        $app['dataService'] = function($c) {
            return new DataService($c['dataModel']);
        };

        $app['userService'] = function($c) {
            return new UserService($c['userModel']);
        };

        $app['dataModel'] = function($c) {
            return new DataModel($c['mySqlDb']);
        };

        $app['userModel'] = function($c) {
            return new UserModel($c['mySqlDb']);
        };

        $app['userProvider'] = function($c) {
            return new UserProvider($c['mySqlDb']);
        };

        $app['mySqlDb'] = function($c){
            $server =
                "mysql:dbname=".$c['mysqlConnection']['dbname']
                .";host=".$c['mysqlConnection']['host']
                .";port=".$c['mysqlConnection']['port']
                .";charset=utf8";
            $pdo = new \PDO($server,$c['mysqlConnection']['user'],$c['mysqlConnection']['password']);
            return $pdo;
        };


        $indexController = $app['indexController'];
        $userController = $app['userController'];
        $controllers = $app['controllers_factory'];


        // Routes - IndexController
        $controllers->get('/', [$indexController, 'indexAction'])->bind('index');

        $controllers->get('/news', [$indexController, 'newsAction'])->bind('news');
        $controllers->get('/news/page/{page}', [$indexController, 'newsPageAction'])->bind('newspage');
        $controllers->get('/news/{article}', [$indexController, 'newsArticleAction'])->bind('newsarticle');


        $controllers->get('/teams', [$indexController, 'teamsAction'])->bind('teams');
        $controllers->get('/teams/{section}/{teamid}', [$indexController, 'teamPageAction'])->bind('teampage');
        $controllers->get('/teams/jugend', [$indexController, 'jugendAction'])->bind('jugend');


        $controllers->get('/verein', [$indexController, 'vereinAction'])->bind('verein');
        $controllers->get('/verein/about-us', [$indexController, 'aboutusAction'])->bind('aboutus');
        $controllers->get('/verein/vorstand', [$indexController, 'vorstandAction'])->bind('vorstand');
        $controllers->get('/verein/trainer', [$indexController, 'trainerAction'])->bind('trainer');
        $controllers->get('/verein/geschichte', [$indexController, 'geschichteAction'])->bind('geschichte');
        $controllers->get('/verein/portrait', [$indexController, 'portraitAction'])->bind('portrait');
        $controllers->get('/verein/vereinsheim', [$indexController, 'vereinsheimAction'])->bind('vereinsheim');
        $controllers->get('/verein/sportplatz', [$indexController, 'sportplatzAction'])->bind('sportplatz');
        $controllers->get('/verein/anfahrt', [$indexController, 'anfahrtAction'])->bind('anfahrt');
        $controllers->get('/verein/termine', [$indexController, 'termineAction'])->bind('termine');
        $controllers->get('/verein/mitgliedschaft', [$indexController, 'mitgliedschaftAction'])->bind('mitgliedschaft');
        $controllers->get('/verein/mitgliedschaft/anmeldung', [$indexController, 'anmeldungAction'])->bind('anmeldung');




        $controllers->get('/trainingszeiten', [$indexController, 'trainingszeitenAction'])->bind('trainingszeiten');

        $controllers->get('/schiedsrichter', [$indexController, 'schiedsrichterAction'])->bind('schiedsrichter');

        $controllers->get('/business', [$indexController, 'businessAction'])->bind('business');
        $controllers->get('/business/sponsoring', [$indexController, 'sponsoringAction'])->bind('sponsoring');
        $controllers->get('/business/sponsor-werden', [$indexController, 'sponsorWerdenAction'])->bind('sponsor_werden');
        $controllers->get('/business/sponsoren', [$indexController, 'sponsorenAction'])->bind('sponsoren');



        $controllers->get('/medien', [$indexController, 'medienAction'])->bind('medien');
        $controllers->get('/medien/bildergalerie', [$indexController, 'bildergalerieAction'])->bind('bildergalerie');
        $controllers->get('/medien/bildergalerie/{year}/{album}', [$indexController, 'albumAction'])->bind('album');
        $controllers->get('/medien/wackertv', [$indexController, 'wackertvAction'])->bind('wackertv');
        $controllers->get('/medien/downloads', [$indexController, 'downloadsAction'])->bind('downloads');


        $controllers->get('/kontakt', [$indexController, 'kontaktAction'])->bind('kontakt');
        #$controllers->post('/kontakt', [$indexController, 'kontaktFormAction'])->bind('kontaktform');
        $controllers->get('/kontakt/impressum', [$indexController, 'impressumAction'])->bind('impressum');
        $controllers->get('/kontakt/links', [$indexController, 'linksAction'])->bind('links');
        $controllers->get('/kontakt/gaestebuch', [$indexController, 'gaestebuchAction'])->bind('gaestebuch');


        $controllers->get('/datenschutz', [$indexController, 'datenschutzAction'])->bind('datenschutz');


        $controllers->get('/ostercup', [$indexController, 'ostercupAction'])->bind('ostercup');
        $controllers->post('/ostercup-registration', [$indexController, 'ostercupRegistration'])->bind('ostercup_registration');


        // Routes - UserController
        $controllers->get('/login', [$userController, 'loginAction'])->bind('login');
        $controllers->match('/registration', [$userController, 'registrationAction'])->bind('registration');
        $controllers->match('/forget-password', [$userController, 'forgetPasswordAction'])->bind('forget_password');
        $controllers->get('/forget-password-sent', [$userController, 'forgetPasswordSentAction'])->bind('forget_password_sent');
        $controllers->get('/account/logout')->bind('logout');
        $controllers->get('/account/profile', [$userController, 'profileAction'])->bind('profile');
        $controllers->match('/account/reset-password/{uniqueId}', [$userController, 'resetPasswordAction'])->bind('reset_password');
        $controllers->match('/account/update-password', [$userController, 'updatePasswordAction'])->bind('update_password');
        $controllers->match('/account/edit-profile-data', [$userController, 'editProfileDataAction'])->bind('edit_profile_data');



        $controllers->post('/deletenewscomment', [$indexController, 'deleteNewsComment']);
        $controllers->get('/getallpicturegalleries', [$indexController, 'getAllPictureGalleries']);
        $controllers->post('/updatevideoviewscounter', [$indexController, 'updateVideoViewsCounter']);


        return $controllers;
    }
}