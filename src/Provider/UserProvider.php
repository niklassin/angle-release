<?php

namespace Angle\Provider;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Angle\Entity\User;

class UserProvider implements UserProviderInterface
{
    private $pdo;

    /**
     * UserProvider constructor.
     * @param \PDO $pdo
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }


    /**
     * @param string $username
     * @return User
     */
    public function loadUserByUsername($username): User
    {
        $stmt = $this->pdo->prepare('SELECT * FROM user WHERE username = :username');
        $stmt->bindParam(':username', $username);
        $stmt->execute();

        if (!$user = $stmt->fetch()) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }

        $userPicture = (int)$user['picture'];
        $userType = $user['user_type'];
        $userId = $user['user_id'];

        return new User($user['username'], $user['password'], explode(',', $user['roles']), true, true, true, true, $userPicture, $userType, $userId);
    }


    /**
     * @param UserInterface $user
     * @return User
     */
    public function refreshUser(UserInterface $user): User
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }


    /**
     * @param string $class
     * @return bool
     */
    public function supportsClass($class): bool
    {
        #return $class === 'Symfony\Component\Security\Core\User\User';
        return $class === 'Angle\Entity\User';
    }
}