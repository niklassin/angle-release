<?php

namespace Angle\Entity;

use Symfony\Component\Security\Core\User\AdvancedUserInterface;

final class User implements AdvancedUserInterface
{
    private $username;
    private $password;
    private $enabled;
    private $accountNonExpired;
    private $credentialsNonExpired;
    private $accountNonLocked;
    private $roles;
    private $userPicture;
    private $userType;
    private $userId;

    /**
     * User constructor.
     * @param $username
     * @param $password
     * @param array $roles
     * @param bool $enabled
     * @param bool $userNonExpired
     * @param bool $credentialsNonExpired
     * @param bool $userNonLocked
     * @param string|null $userPicture
     * @param string|null $userType
     * @param int $userId
     */
    public function __construct($username, $password, array $roles = array(), $enabled = true, $userNonExpired = true, $credentialsNonExpired = true, $userNonLocked = true, string $userPicture = null, string $userType = null, int $userId)
    {
        if ('' === $username || null === $username) {
            throw new \InvalidArgumentException('The username cannot be empty.');
        }

        $this->username = $username;
        $this->password = $password;
        $this->enabled = $enabled;
        $this->accountNonExpired = $userNonExpired;
        $this->credentialsNonExpired = $credentialsNonExpired;
        $this->accountNonLocked = $userNonLocked;
        $this->roles = $roles;
        $this->userPicture = $userPicture;
        $this->userType = $userType;
        $this->userId = $userId;
    }


    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getUsername();
    }


    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }


    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }


    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {
    }


    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }


    /**
     * @return bool
     */
    public function isAccountNonExpired(): bool
    {
        return $this->accountNonExpired;
    }


    /**
     * @return bool
     */
    public function isAccountNonLocked(): bool
    {
        return $this->accountNonLocked;
    }


    /**
     * @return bool
     */
    public function isCredentialsNonExpired(): bool
    {
        return $this->credentialsNonExpired;
    }


    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }


    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
    }


    /**
     * @return string
     */
    public function getUserPicture(): string
    {
        return $this->userPicture;
    }


    /**
     * @return string
     */
    public function getUserType(): string
    {
        return $this->userType;
    }


    public function getUserId(): int
    {
        return $this->userId;
    }

}