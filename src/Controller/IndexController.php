<?php

namespace Angle\Controller;

use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Angle\Service\DataService;
use Angle\Service\UserService;
use Angle\Entity\User;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class IndexController
{
    private $dataService;
    private $userService;

    public function __construct(DataService $dataService, UserService $userService)
    {
        $this->dataService = $dataService;
        $this->userService = $userService;
    }


    public function indexAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());
        $lastNews = $this->dataService->getLastFiveNews();
        $lastNewsConverted = $this->dataService->convertNews($lastNews);
        $newsData = $this->dataService->getNewsByLimit(6, 0);
        $news = $this->dataService->convertNews($newsData);
        $lastGameResults = $this->dataService->getAllGames();

        $unformatedFeed = (array)$this->dataService->getInstagramFeed();
        $feed = $this->dataService->formatInstagramFeed($unformatedFeed['medias']);

        return $app['twig']->render('index.html.twig', array(
            'urlPath' => $urlPath,
            'lastGameResults' => $lastGameResults,
            'lastNews' => $lastNewsConverted,
            'newsData' => $news,
            'feed' => $feed
        ));
    }


    public function newsAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());
        $newsData = $this->dataService->getNewsByLimit(12, 0);
        $pagesNumber = $this->dataService->getNewsPagesNumber();

        return $app['twig']->render('news/news.html.twig', array(
            'urlPath' => $urlPath,
            'newsData' => $newsData,
            'pagesNumber' => $pagesNumber,
            'activePage' => 1
        ));
    }


    public function newsPageAction(Application $app, Request $request, $page)
    {
        $offset = (int)$page * 12 - 12;
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());
        $newsData = $this->dataService->getNewsByLimit(12, $offset);
        $pagesNumber = $this->dataService->getNewsPagesNumber();

        return $app['twig']->render('news/news.html.twig', array(
            'urlPath' => $urlPath,
            'newsData' => $newsData,
            'pagesNumber' => $pagesNumber,
            'activePage' => $page
        ));
    }


    public function newsArticleAction(Application $app, Request $request, $article)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());
        $newsArticle = $this->dataService->getNewsArticleByUrl($article);
        #$tags = explode(',', $newsArticle['tags']);
        $tags = [];
        $topNews = $this->dataService->getTopNewsWithCommentsByLimit(5);
        $comments = $this->dataService->getNewsComments($newsArticle['news_id']);
        $this->dataService->updateNewsArticleCounter((int)$newsArticle['news_id']);

        return $app['twig']->render('news/news-article.html.twig', array(
            'urlPath' => $urlPath,
            'newsArticle' => $newsArticle,
            'tags' => $tags,
            'topNews' => $topNews,
            'comments' => $comments
        ));
    }


    public function portraitAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());

        return $app['twig']->render('verein/portrait.html.twig', array(
            'urlPath' => $urlPath
        ));
    }

    public function bildergalerieAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());
        $data = $this->dataService->getAllPictureGalleries();

        return $app['twig']->render('medien/bildergalerie.html.twig', array(
            'urlPath' => $urlPath,
            'galleries' => $data
        ));
    }

    public function albumAction(Application $app, Request $request, $year, $album)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());
        $urlPathLastLink = $urlPath[count($urlPath)-1];
        $pictures = $this->dataService->getGalleryAlbumPictures($year, $album);
        $pictures = str_replace('../public/', '', $pictures);
        $albumInfos = $this->dataService->getAlbumInfosByNameAndYear($album, $year);

        unset($urlPath[count($urlPath)-1]);

        return $app['twig']->render('medien/album.html.twig', array(
            'urlPath' => $urlPath,
            'urlPathLastLink' => $urlPathLastLink,
            'pictures' => $pictures,
            'year' => $year,
            'path' => $album,
            'album_infos' => $albumInfos
        ));
    }


    public function ostercupAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());
        $registrationStatus = "off";

        return $app['twig']->render('ostercup.html.twig', array(
            'urlPath' => $urlPath,
            'registrationStatus' => $registrationStatus
        ));
    }


    public function linksAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());

        return $app['twig']->render('kontakt/links.html.twig', array(
            'urlPath' => $urlPath
        ));
    }


    public function aboutusAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());
        $data = $this->dataService->getAllWackerMembers();

        return $app['twig']->render('verein/aboutus.html.twig', array(
            'urlPath' => $urlPath,
            'members' => $data,
            'video' => 'off'
        ));
    }


    public function geschichteAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());

        return $app['twig']->render('verein/geschichte.html.twig', array(
            'urlPath' => $urlPath
        ));
    }

    public function trainingszeitenAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());
        $data = $this->dataService->getTrainingTimes();

        return $app['twig']->render('trainingszeiten.html.twig', array(
            'urlPath' => $urlPath,
            'trainingSeniorenDamen' => $data['seniorenDamen'],
            'trainingJugend' => $data['jugend']
        ));
    }

    public function impressumAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());

        return $app['twig']->render('kontakt/impressum.html.twig', array(
            'urlPath' => $urlPath
        ));
    }

    public function jugendAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());
        $data = $this->dataService->getAllVorstandMembers();
        $teams = $this->dataService->getTrainingTimes();

        return $app['twig']->render('teams/jugend.html.twig', array(
            'urlPath' => $urlPath,
            'jugendvorstand' => $data['jugendvorstand'],
            'teams' => $teams['jugend']
        ));
    }

    public function vereinsheimAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());
        $data = [];

        $form = $app['form.factory']->createBuilder(FormType::class, $data)
            ->add('username', TextType::class, [
                'label' => 'Name',
                'constraints' => [
                    new Length(['min' => 1, 'max' => '255']),
                    new NotBlank()
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => 'E-Mailadresse',
                'constraints' => [
                    new Length(['min' => 6, 'max' => 200]),
                    new NotBlank()
                ]
            ])
            ->add('date', DateType::class, [
                'label' => 'Vsl. Termin',
                'format' => 'dd-MMMM-yyyy'
            ])
            ->add('Typ', ChoiceType::class, [
                'label' => 'Typ',
                'choices'  => array(
                    'Geburtstag' => null,
                    'Hochzeit' => true,
                    'Jubiläum' => false,
                    'Sonstige Veranstaltung'
                ),
            ])
            ->add('Nachricht', TextareaType::class, [
                'label' => 'Nachricht',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Speichern',
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $formData = $form->getData();
            $username = filter_var($formData['username'], FILTER_SANITIZE_STRING);
            $email = filter_var($formData['email'], FILTER_SANITIZE_EMAIL);
            $contactFirstName = filter_var($formData['contact_first_name'], FILTER_SANITIZE_STRING);
            $contactLastName = filter_var($formData['contact_last_name'], FILTER_SANITIZE_STRING);
            $userId = 1;

            if ($form->isValid()) {
                $updateProfileData = $this->userService->updateProfileData($username, $email, $userId);

                if ($updateProfileData) {
                    return $app['twig']->render('verein/vereinsheim.html.twig', array(
                        'form' => $form->createView(),
                        'urlPath' => $urlPath,
                        'success' => "Die Profildaten wurden erfolgreich gespeichert.",
                        'path' => $request->getUri()
                    ));
                }
                else {
                    return $app['twig']->render('verein/vereinsheim.html.twig', array(
                        'form' => $form->createView(),
                        'urlPath' => $urlPath,
                        'error' => "Benutzername oder E-Mailadresse existieren bereits. Bitte noch einmal versuchen."
                    ));
                }
            }
            else {
                return $app['twig']->render('verein/vereinsheim.html.twig', array(
                    'form' => $form->createView(),
                    'urlPath' => $urlPath
                ));
            }
        }

        return $app['twig']->render('verein/vereinsheim.html.twig', array(
            'form' => $form->createView(),
            'urlPath' => $urlPath
        ));
    }


    public function vorstandAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());
        $data = $this->dataService->getAllVorstandMembers();

        return $app['twig']->render('verein/vorstand.html.twig', array(
            'urlPath' => $urlPath,
            'hauptvorstand' => $data['hauptvorstand'],
            'jugendvorstand' => $data['jugendvorstand'],
            'weitere_aemter' => $data['weitere_aemter']
        ));
    }


    public function trainerAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());
        $data = $this->dataService->getAllTrainer();

        return $app['twig']->render('verein/trainer.html.twig', array(
            'urlPath' => $urlPath,
            'seniorenTrainer' => $data['senioren_trainer'],
            'jugendTrainer' => $data['jugend_trainer']
        ));
    }


    public function sportplatzAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());

        return $app['twig']->render('verein/sportplatz.html.twig', array(
            'urlPath' => $urlPath,
        ));
    }


    public function anfahrtAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());

        return $app['twig']->render('verein/anfahrt.html.twig', array(
            'urlPath' => $urlPath,
        ));
    }

    public function termineAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());

        return $app['twig']->render('verein/termine.html.twig', array(
            'urlPath' => $urlPath,
        ));
    }

    public function mitgliedschaftAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());

        return $app['twig']->render('verein/mitgliedschaft.html.twig', array(
            'urlPath' => $urlPath,
        ));
    }

    public function anmeldungAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());

        return $app['twig']->render('verein/anmeldung.html.twig', array(
            'urlPath' => $urlPath,
        ));
    }

    public function wackertvAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());
        $data = $this->dataService->getAllVideos();

        return $app['twig']->render('medien/wackertv.html.twig', array(
            'urlPath' => $urlPath,
            'videos' => $data
        ));
    }

    public function downloadsAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());

        return $app['twig']->render('medien/downloads.html.twig', array(
            'urlPath' => $urlPath
        ));
    }



    public function kontaktAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());

        return $app['twig']->render('kontakt/kontakt.html.twig', array(
            'urlPath' => $urlPath
        ));
    }

    public function kontaktFormAction(Application $app, Request $request)
    {
        $username = $request->request->get('username');
        $email = $request->request->get('email');
        $message = $request->request->get('message');

        $result = $this->dataService->insertContactData($username, $email, $message);

        if(is_array($result) && count($result['errors']) >= 1) {
            return $app['twig']->render('kontakt/kontakt.html.twig', array(
                'errors' => $result['errors'],
                'postData' => $result['data']
            ));
        }
        else {
            $this->dataService->sendContactFormMail($username, $email, $message);

            return $app['twig']->render('kontakt/kontakt.html.twig', array(
                'success' => 'Nachricht erfolgreich abgeschickt!'
            ));
        }
    }

    public function schiedsrichterAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());

        return $app['twig']->render('schiedsrichter.html.twig', array(
            'urlPath' => $urlPath
        ));
    }


    public function teamPageAction(Application $app, Request $request, $section, $teamid)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());

        $player = $this->dataService->getTeamPlayer($teamid);
        $teamPage = $this->dataService->getTeamPage($teamid);
        $trainingTimes = $this->dataService->getTeamTrainingTimes($teamid);
        $trainer = $this->dataService->getTrainerByTeam($teamid);
        $teamLeagueStatistics = $this->dataService->getTeamLeagueStatistics($teamid);

        return $app['twig']->render('teams/teampage.html.twig', array(
            'urlPath' => $urlPath,
            'teamPage' => $teamPage,
            'teamPlayer' => $player,
            'trainingTimes' => $trainingTimes,
            'trainers' => $trainer,
            'section' => $section,
            'teamLeagueStatistics' => $teamLeagueStatistics
        ));
    }

    public function sponsoringAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());

        return $app['twig']->render('business/sponsoring.html.twig', array(
            'urlPath' => $urlPath
        ));
    }

    public function sponsorWerdenAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());

        return $app['twig']->render('business/sponsor-werden.html.twig', array(
            'urlPath' => $urlPath
        ));
    }

    public function sponsorenAction(Application $app, Request $request)
    {
        $urlPath = $this->dataService->splitUrlPath($request->getRequestUri());

        return $app['twig']->render('business/sponsoren.html.twig', array(
            'urlPath' => $urlPath
        ));
    }



    public function deleteNewsComment(Application $app, Request $request)
    {
        try {
            $token = $app['security.token_storage']->getToken();
            $user = $this->getUserData($token);

            $newsId = (int)$request->request->get('newsId');
            $commentId = (int)$request->request->get('commentId');

            $checkUserCommentExists = $this->dataService->checkNewsCommentByUserExists($newsId, $commentId, $user->getUserId());

            if($checkUserCommentExists) {
                $deleteUserComment = $this->dataService->deleteNewsCommentByUser($newsId, $commentId, $user->getUserId());

                if($deleteUserComment)
                    return $app->json([], 200);
                else
                    return $app->json('Fehler beim Löschen des Kommentars', 400);
            }
        }
        catch(\Exception $e) {
            $error = ['message' => $e->getMessage()];
            return $app->json($error, $e->getCode());
        }
    }


    public function datenschutzAction(Application $app, Request $request)
    {

    }



    public function getAllPictureGalleries(Application $app)
    {
        try {
            $data = $this->dataService->getAllPictureGalleries();
            return $app->json($data, 200);
        }
        catch(\Exception $e) {
            $error = ['message' => $e->getMessage()];
            return $app->json($error, $e->getCode());
        }
    }



    public function ostercupRegistration(Application $app, Request $request)
    {
        try {
            $clubName = $request->request->get('club_name');
            $team = $request->request->get('team');
            $playerCount = $request->request->get('player_count');
            $contactPersonName = $request->request->get('contact_person_name');
            $contactPersonEmail = $request->request->get('contact_person_email');
            $city = $request->request->get('city');
            $acceptDataProtectionRegulations = $request->request->get('accept_data_protection_regulations');

            $data = $this->dataService->insertOstercupRegistration($clubName, $team, $playerCount, $contactPersonName, $contactPersonEmail, $city);
            return $app->json($data, 200);
        }
        catch(\Exception $e) {
            $error = ['message' => $e->getMessage()];
            return $app->json($error, $e->getCode());
        }
    }



    /**
     * Aktiven absoluten Pfad der URL holen
     *
     * @param string $path
     * @return string
     */
    public function getActivepath(string $path): string
    {
        if (strpos($path, 'home') !== false) {
            return 'home';
        }
        else if (strpos($path, 'statistiken') !== false) {
            return 'statistiken';
        }
        if (strpos($path, 'account') !== false) {
            return 'account';
        }
    }


    /**
     * Prüfen ob User eingeloggt
     *
     * @param $token
     * @return bool
     */
    public function checkLogin($token): bool
    {
        if ($token->getUser() instanceof User)
            return true;
        else return false;
    }


    /**
     * Benutzerdaten holen
     *
     * @param $token
     * @return User
     */
    public function getUserData($token): User
    {
        return $token->getUser();
    }


    public function updateVideoViewsCounter(Application $app, Request $request)
    {
        try {
            $videoId = $request->request->get('video_id');
            $data = $this->dataService->updateVideoViewsCounter((int)$videoId);

            return $app->json($data, 200);
        }
        catch(\Exception $e) {
            $error = ['message' => $e->getMessage()];
            return $app->json($error, $e->getCode());
        }
    }

}