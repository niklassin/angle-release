<?php

namespace Angle\Controller;

use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Angle\Service\UserService;
use Angle\Entity\User;

class UserController
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }


    public function registrationAction(Application $app, Request $request)
    {
        $token = $app['security.token_storage']->getToken();
        $checkLogin = $this->checkLogin($token);

        if ($checkLogin === false) {
            $data = [];

            $form = $app['form.factory']->createBuilder(FormType::class, $data)
                ->add('username', TextType::class, [
                    'label' => false,
                    'attr' => [
                        'placeholder' => 'Benutzername',
                        'class' => 'form-control'
                    ],
                    'constraints' => [
                        new Length(['min' => 5, 'max' => 100])
                    ]
                ])
                ->add('email', EmailType::class, [
                    'label' => false,
                    'attr' => [
                        'placeholder' => 'E-Mailadresse',
                        'class' => 'form-control'
                    ],
                    'constraints' => [
                        new Length(['min' => 6, 'max' => 200])
                    ]
                ])
                ->add('password', PasswordType::class, [
                    'label' => false,
                    'constraints' => [
                        new Length(['min' => 5, 'max' => 100])
                    ],
                    'attr' => [
                        'placeholder' => 'Passwort',
                        'class' => 'form-control'
                    ]
                ])
                ->add('password_wdh', PasswordType::class, [
                    'label' => false,
                    'constraints' => [
                        new Length(['min' => 5, 'max' => 100])
                    ],
                    'attr' => [
                        'placeholder' => 'Passwort wiederholen',
                        'class' => 'form-control'
                    ]
                ])
                ->add('submit', SubmitType::class, [
                    'label' => 'Registrieren',
                    'attr' => ['class' => 'btn btn-primary reset-password-btn']
                ])
                ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted()) {
                if ($form->isValid()) {
                    $data = $form->getData();
                    $email = $data['email'];
                    $username = $data['username'];

                    $checkUsernameEmail = $this->userService->checkUserNameOrEmailExist($username, $email);

                    if($checkUsernameEmail['username'] === false && $checkUsernameEmail['email'] === false) {
                        return $app['twig']->render('account/registration-success.html.twig');
                    }
                    else {
                        if($checkUsernameEmail['username'] === true && $checkUsernameEmail['email'] === false) {
                            return $app['twig']->render('account/registration.html.twig', array(
                                'form' => $form->createView(),
                                'formError' => 'Eingegebener Benutzername existiert bereits!'
                            ));
                        }
                        else if($checkUsernameEmail['username'] === false && $checkUsernameEmail['email'] === true) {
                            return $app['twig']->render('account/registration.html.twig', array(
                                'form' => $form->createView(),
                                'formError' => 'Eingegebene E-Mailadresse existiert bereits!'
                            ));
                        }
                        else {
                            return $app['twig']->render('account/registration.html.twig', array(
                                'form' => $form->createView(),
                                'formError' => 'Eingegebener Benutzername oder E-Mailadresse existieren bereits!'
                            ));
                        }
                    }
                }
                else {
                    return $app['twig']->render('account/registration.html.twig', array(
                        'form' => $form->createView()
                    ));
                }
            }

            return $app['twig']->render('account/registration.html.twig', array(
                'form' => $form->createView()
            ));
        }
        else return $app->redirect($app['url_generator']->generate('account_home'));
    }


    /**
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function loginAction(Application $app, Request $request)
    {
        $token = $app['security.token_storage']->getToken();
        $checkLogin = $this->checkLogin($token);

        if ($checkLogin === false) {
            return $app['twig']->render('account/login.html.twig', array(
                'error' => $app['security.last_error']($request),
                'last_username' => $app['session']->get('_security.last_username'),
            ));
        }
        else return $app->redirect($app['url_generator']->generate('news'));
    }


    /**
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function forgetPasswordAction(Application $app, Request $request)
    {
        $token = $app['security.token_storage']->getToken();
        $checkLogin = $this->checkLogin($token);

        if ($checkLogin === false) {
            $data = [];

            $form = $app['form.factory']->createBuilder(FormType::class, $data)
                ->add('email', EmailType::class, [
                    'label' => false,
                    'attr' => [
                        'placeholder' => 'E-Mailadresse',
                        'class' => 'form-control'
                    ],
                    'constraints' => [
                        new Length(['min' => 6, 'max' => 200])
                    ]
                ])
                ->add('submit', SubmitType::class, [
                    'label' => 'Passwort zurücksetzen',
                    'attr' => ['class' => 'btn btn-primary reset-password-btn']
                ])
                ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted()) {
                if ($form->isValid()) {
                    $data = $form->getData();
                    $email = $data['email'];

                    $checkEmail = $this->userService->forgetPasswordCheck($email);

                    if ($checkEmail) {
                        $companyData = $this->userService->getUserDataByEmail($email);

                        return $app['twig']->render('account/forget-password-sent.html.twig', array(
                            'passwordCode' => $companyData['link']
                        ));
                    }
                    else {
                        return $app['twig']->render('account/forget-password.html.twig', array(
                            'form' => $form->createView(),
                            'formError' => 'Eingegebene E-Mailadresse ungültig oder existiert nicht!'
                        ));
                    }
                }
                else {
                    return $app['twig']->render('account/forget-password.html.twig', array(
                        'form' => $form->createView()
                    ));
                }
            }

            return $app['twig']->render('account/forget-password.html.twig', array(
                'form' => $form->createView()
            ));
        }
        else return $app->redirect($app['url_generator']->generate('account_home'));
    }








    /**
     * Prüfen ob User eingeloggt
     *
     * @param $token
     * @return bool
     */
    public function checkLogin($token): bool
    {
        if ($token->getUser() instanceof User)
            return true;
        else return false;
    }


    /**
     * Benutzerdaten holen
     *
     * @param $token
     * @return User
     */
    public function getUserData($token): User
    {
        return $token->getUser();
    }

}