jQuery(document).read(function($) {

    var chart = AmCharts.makeChart( "chartdiv", {
        "type": "xy",
        "theme": "black",
        "dataProvider": [ {
            "ax": 0,
            "ay": 3
        }, {
            "ax": 1,
            "ay": 1
        }, {
            "ax": 2,
            "ay": 0
        }, {
            "ax": 3,
            "ay": 3
        }, {
            "ax": 4,
            "ay": 3
        }],
        "valueAxes": [ {
            "position": "bottom",
            "axisAlpha": 0,
            "dashLength": 1,
            "id": "x",
            "title": "X Axis"
        }, {
            "axisAlpha": 0,
            "dashLength": 1,
            "position": "left",
            "id": "y",
            "title": "Y Axis"
        } ],
        "startDuration": 0,
        "graphs": [ {
            "balloonText": "x:[[x]] y:[[y]]",
            "fillAlphas": 0.3,
            "fillToAxis": "x",
            "lineAlpha": 1,
            "xField": "ax",
            "yField": "ay",
            "lineColor": "#FF6600"
        }],
        "marginLeft": 64,
        "marginBottom": 60,
        "chartScrollbar": {},
        "chartCursor": {},
        "export": {
            "enabled": true,
            "position": "bottom-right"
        }
    } );

});