jQuery(document).ready(function($) {

    $('.news-box').on('click', function() {
        var video = $(this).data('video');
        var title = $(this).data('title');
        var video_id = $(this).data('video_id');

        $.sweetModal({
            title: title,
            content: video,
            theme: $.sweetModal.THEME_DARK
        });

        updateViewsCounterAjax(video_id);
    });

    function updateViewsCounterAjax(video_id)
    {
        $.ajax({
            url: "/updatevideoviewscounter",
            data: {video_id: video_id},
            type: "POST"
        }).done(function(result) {
            console.log(result);
            console.log("OKK");
        });
    }

});
