jQuery(document).ready(function($) {

    $(document).ready(function() {
        $('#gallery-stadion').lightGallery({
            thumbnail:true
        });

        var options = {
            beforeSubmit:  showRequest,  // pre-submit callback
            success:       showResponse,  // post-submit callback
            error: showError

            // other available options:
            //url:       url         // override for form's 'action' attribute
            //type:      type        // 'get' or 'post', override for form's 'method' attribute
            //dataType:  null        // 'xml', 'script', or 'json' (expected server response type)
            //clearForm: true        // clear all form fields after successful submit
            //resetForm: true        // reset the form after successful submit

            // $.ajax options can be used here too, for example:
            //timeout:   3000
        };

        // bind to the form's submit event
        $('#ostercup-registration').submit(function() {
            // inside event callbacks 'this' is the DOM element so we first
            // wrap it in a jQuery object and then invoke ajaxSubmit
            $(this).ajaxSubmit(options);

            // !!! Important !!!
            // always return false to prevent standard browser submit and page navigation
            return false;
        });
    });

    // pre-submit callback
    function showRequest(formData, jqForm, options) {

        var value = $('#accept-data-protection-regulations').fieldValue();

        if(value.length < 1) {
            return false;
        }

        var form = jqForm[0];
        //console.log(form.accept_data_protection_regulations.value);
        /*if (!form.username.value || !form.password.value) {
            alert('Please enter a value for both Username and Password');
            return false;
        }*/
        alert('Both fields contain values.');









        // formData is an array; here we use $.param to convert it to a string to display it
        // but the form plugin does this for you automatically when it submits the data
        //var queryString = $.param(formData);

        // jqForm is a jQuery object encapsulating the form element.  To access the
        // DOM element for the form do this:
        // var formElement = jqForm[0];

        //alert('About to submit: \n\n' + queryString);

        // here we could return false to prevent the form from being submitted;
        // returning anything other than false will allow the form submit to continue
        return true;
    }

    // post-submit callback
    function showResponse()  {
        swal({
            title: 'Anmeldung erfolgreich',
            text: 'Die Anmeldung für unseren nächsten Ostercup ist bei uns eingegangen und wird zeitnah bearbeitet. Im Anschluss daran melden wir uns per E-Mai bei dir.',
            type: 'success',
            showCancelButton: false,
            confirmButtonText: 'Schließen'
        });
    }

    function showError() {
        swal({
            title: 'Anmeldung fehlerhaft',
            text: 'Es ist ein Fehler bei der Anmeldung aufgetreten. Bitte versuche die Anmeldung noch einmal.',
            type: 'error',
            showCancelButton: false,
            confirmButtonText: 'Schließen'
        });
    }

});