jQuery(document).ready(function($) {


    if($(window).width() <= 991) {
        $('#sidebar').addClass('sidebar-mobile');
        $('#content').addClass('content-mobile');

        $('#sidebar-btn').on('click', function() {
            $('#sidebar').toggleClass('sidebar-small').toggleClass('sidebar-mobile');
            $('#nav-logo-box').toggleClass('nav-logo-box-small');
            $('#content').toggleClass('content-big').toggleClass('content-mobile');
        });
    }
    else {
        $('ul.navbar-nav li.dropdown, ul.nav li.dropdown-submenu').hover(function() {
            $(this).find(' > .dropdown-menu').stop(true, true).delay(200).slideDown();
        }, function() {
            $(this).find(' > .dropdown-menu').stop(true, true).slideUp();
        });

        $('#sidebar-btn').on('click', function() {
            $('#sidebar').toggleClass('sidebar-small');
            $('#nav-logo-box').toggleClass('nav-logo-box-small');
            $('#content').toggleClass('content-big');
        });
    }


    $('#sidebar .nav-item').on('click', function() {

        var elem = $(this).find('.collapse.show');

        if(elem.length === 1) {
            $('#sidebar .nav-item').removeClass('nav-item-active');
            $('#sidebar .nav-item').find('.nav-link').removeClass('link-active');
        }
        else {
            $('#sidebar .nav-item').removeClass('nav-item-active');
            $('#sidebar .nav-item').find('.nav-link').removeClass('link-active');
            $(this).addClass('nav-item-active');
            $(this).find('.nav-link').addClass('link-active');
        }
    });



    $('#sidebar .nav-item').on('mouseover', function() {
        $(this).addClass('nav-item-active');
        $(this).find('.nav-link').addClass('link-active');
    });

    $('#sidebar .nav-item').on('mouseleave', function() {
        $('#sidebar .nav-item').removeClass('nav-item-active');
        $('#sidebar .nav-item').find('.nav-link').removeClass('link-active');
    });



    $('a[href*="#"]').on('click', function (e) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top - 200
        }, 500, 'linear');
    });


});