jQuery(document).ready(function($) {

    if($(window).width() >= 992) {
        $("#menu li a").hover(function() {
            $(this).stop().animate({
                width: "130px",
                height: "130px",

                left: $(this).css("left")-40,
                lineHeight: "130px"
            }, 400);
        }, function() {
            $(this).stop().animate({
                width: "120px",
                height: "120px",
                lineHeight: "120px"
            }, 400);
        });
    }

});