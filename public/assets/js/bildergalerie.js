jQuery(document).ready(function($) {

    $(function() {

        loadGalleries();

        $('#sel1').on('change', function() {
            var sortParam = $(this).find('option:selected').val();

            sortGalleriesByParam(sortParam);
        })

    });

    function loadGalleriesAjax() {
        return new Promise(function(resolve,reject){
            $.ajax({
                url: "/getallpicturegalleries",
                type: "GET",
                dataType: "json",
            }).done(function(result) {
                resolve(result);
            }).fail(function (failReason){
                reject(failReason);
            });
        });
    }

    function loadGalleries() {
        loadGalleriesAjax().then(function(result) {
            setGalleriesData(result);
        });
    }

    function setGalleriesData(data) {
        var gallery = '';

        $.each(data, function(galleryKey, galleryObj) {
            gallery += '<div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-xs-12 boxx" data-date="'+galleryObj.created_date+'">';
            gallery += '<a href="/medien/bildergalerie/'+galleryObj.year+'/'+galleryObj.path+'">';
            gallery += '<div class="card-box gallery-box">';
            gallery += '<div class="gallery-box-content">';
            gallery += '<img src="'+galleryObj.random_thumbnail+'" alt="'+galleryObj.album_title+'" title="'+galleryObj.album_title+'" />';
            gallery += '<div class="gallery-box-headline">';
            gallery += '<h3>'+galleryObj.album_title+'</h3>';
            gallery += '</div>';
            gallery += '</div>';
            gallery += '</div>';
            gallery += '</a>';
            gallery += '</div>';
        });

        $('#galleries-container').html(gallery);
    }

    function sortGalleriesByParam(sortParam) {
        var $divs = $("div.boxx");
        if(sortParam === 'date_asc') {
            var alphabeticallyOrderedDivs = $divs.sort(function (a, b) {
                return $(a).data('date') > $(b).data('date');
            });

            $('#galleries-container').html(alphabeticallyOrderedDivs);
        }
        else {
            var alphabeticallyOrderedDivs = $divs.sort(function (a, b) {
                return $(a).data('date') < $(b).data('date');
            });

            $('#galleries-container').html(alphabeticallyOrderedDivs);
        }
    }

});