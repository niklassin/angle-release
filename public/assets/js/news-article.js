jQuery(document).ready(function($) {

    $('.delete-comment').on('click', function() {
        var $this = $(this);
        var commentId = $this.data('commentid');
        var newsId = $this.data('newsid');

        swal({
            title: 'Kommentar löschen',
            inputPlaceholder: 'Select country',
            html: "Der zu löschende Kommentar wird endgültig gelöscht und nicht mehr zu sehen sein!",
            showCancelButton: true,
            confirmButtonText: 'Löschen',
            cancelButtonText: 'Abbrechen',
            showCloseButton: true,
            showLoaderOnConfirm: false,
            allowOutsideClick: false
        }).then(function(result) {
            if(result.value) {
                $.ajax({
                    url: "/deletenewscomment",
                    type: "POST",
                    data: {commentId: commentId, newsId: newsId}
                }).done(function() {
                    $('#comment-'+commentId).remove();
                    swal({
                        type: 'success',
                        title: 'Stornierungsbeanstandung erfolgreich',
                        text: 'Die Stornierungsbeanstandung der Anfrage wurde erfolgreich gespeichert und wird von uns bearbeitet!',
                        showConfirmButton: true,
                        confirmButtonText: 'Schließen',
                        showCancelButton: false
                    });
                }).fail(function(json) {
                    getErrorMessage(json);
                });
            }
        });
    });


    $("#user-comment-form").submit(function(e){
        e.preventDefault();

        var comment = $('#user-comment');

        comment.on('input change keyup', function() {
            if($.trim(comment.val()) === '') {
                $('#user-comment').addClass('user-comment-error');
                $('#comment-error').text('Bitte einen Kommentar eingeben!');
            }
            else {
                $('#user-comment').removeClass('user-comment-error');
                $('#comment-error').text('');
            }
        });
    });


    function getErrorMessage(failReason) {
        swal({
            type: 'error',
            title: 'Fehler',
            html: failReason.responseJSON.message,
            showConfirmButton: true,
            confirmButtonText: 'Schließen',
            showCancelButton: false,
            animation: true
        });
    }

});