<?php

#session_save_path(realpath(dirname($_SERVER['DOCUMENT_ROOT'])) . '/httpdocs/var/sessions');
ini_set('display_errors', 0);

require_once __DIR__.'/../vendor/autoload.php';

use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\SecurityServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\LocaleServiceProvider;
use Silex\Provider\RememberMeServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Silex\Provider\RoutingServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Angle\Provider\RoutesControllerProvider;
use Angle\Provider\UserProvider;


$config = require_once __DIR__ . '/../config/application.config.php';

$app = new Application();
$app['debug'] = true;
$app['config'] = $config;
$app['mysqlConnection'] = $config['mysqlConnection'];

$app->register(new ServiceControllerServiceProvider());
$app->register(new RoutingServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/../views',
    'twig.class_path' => __DIR__ . '/../vendor/Twig/lib',
    'twig.options' => array('cache' => __DIR__ . '/../var/cache')
));
$app->register(new TranslationServiceProvider(), array(
    'translator.messages' => array(),
));
$app->register(new ValidatorServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app->register(new LocaleServiceProvider());
$app->register(new FormServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app->register(new SessionServiceProvider());
$app->register(new SecurityServiceProvider(), array(
    'security.firewalls' => [
        'general' => [
            'pattern' => '^/.*$',
            'anonymous' => true,
            'form' => [
                'login_path' => '/login',
                'check_path' => '/account/login_check',
                'always_use_default_target_path' => true,
                'default_target_path' => '/news'
            ],
            'logout' => [
                'logout_path' => '/account/logout',
                'target_url' => '/login',
                'invalidate_session' => true
            ],
            /*'remember_me' => array(
                'key'                => 'Choose_A_Unique_Random_Key',
                'always_remember_me' => true,
            ),*/
            'users' => function () use ($app) {
                return new UserProvider($app['mySqlDb']);
            },
            /*'users' => [
                'admin' => ['ROLE_ADMIN', '$2y$13$pZoJI/9L5OzrTEwN9UhdzeY7htZoSw7MalWO7gu2HlFnWir/Scsw2'], // password => foo
            ]*/
        ],
        /*'secured_area' => array(
            'pattern' => '^/account/',
            'anonymous' => false,
        ),*/
    ],
    /*'security.access_rules' => [
        ['^/account', 'ROLE_USER']
    ],*/
    'security.role_hierarchy' => [
        'ROLE_ADMIN' => [
            'ROLE_USER',
            'ROLE_ALLOWED_TO_SWITCH'
        ],
    ]
));


$app['twig'] = $app->extend('twig', function ($twig, $app) {
    // add custom globals, filters, tags, ...

    return $twig;
});


$app->mount("/", new RoutesControllerProvider());


$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html.twig',
        'errors/'.substr($code, 0, 2).'x.html.twig',
        'errors/'.substr($code, 0, 1).'xx.html.twig',
        'errors/default.html.twig',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});

$app->run();
